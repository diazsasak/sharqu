<?php

use Illuminate\Database\Seeder;

use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //hapus semua data supaya ndk conflik
    	DB::statement("SET foreign_key_checks=0");
    	User::truncate();
    	DB::statement("SET foreign_key_checks=1");

    	$owner = User::create(
    		[
    		'email' => 'testing@t.com',
    		'password' => bcrypt('testing')
    		]
    		);
    }
}
