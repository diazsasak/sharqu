@extends('layout')
@section('title')
Login
@endsection
@section('head')
<link href="<% asset("dist/css/login.css") %>" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container">
  <div class="row">

    <div class="main">

      <h3>Silahkan Masuk, atau <a href="<% URL::to('register') %>">Daftar</a></h3>
      <form role="form" method="POST" action="<% URL::to('/login') %>">
      <!! csrf_field() !!>
        <div class="form-group">
          <label for="inputUsernameEmail">email</label>
          <input type="email" name="email" class="form-control" value="<% old('email') %>">
        </div>
        <div class="form-group">
          <label for="inputPassword">Password</label>
          <input type="password" class="form-control" name="password" id="password">
        </div>
        <div class="checkbox pull-right">
          <label>
            <input type="checkbox" name="remember">
            Tetap login pada perangkat ini </label>
        </div>
        <button type="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon-log-in"></span>
          Login
        </button>
      </form>
    
    </div>
    
  </div>
</div>
@endsection