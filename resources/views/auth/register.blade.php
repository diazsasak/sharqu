@extends('layout')
@section('title')
Daftar
@endsection
@section('content')
<div ng-controller="RegisterCtrl">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-left">
			<form name="forms.registerForm" id="registerForm" novalidate="" method="POST" enctype="multipart/form-data" action="<% URL::to('/register') %>">
				<!! csrf_field() !!>
				<div class="form-group">
					<label>Nama depan</label>
					<input type="text" ng-model="inputData.first_name" name="first_name" maxlength="20" class="form-control" required>
					<div class="error-container" ng-show="forms.registerForm.first_name.$error && forms.registerForm.first_name.$dirty" ng-messages="forms.registerForm.first_name.$error">
						<div ng-messages-include="error-list.html"></div>
					</div>
				</div>
				<div class="form-group">
					<label>Nama belakang</label>
					<input type="text" ng-model="inputData.last_name" name="last_name" class="form-control" required>
					<div class="error-container" ng-show="forms.registerForm.last_name.$error && forms.registerForm.last_name.$dirty" ng-messages="forms.registerForm.last_name.$error">
						<div ng-messages-include="error-list.html"></div>
					</div>
				</div>
				<div class="form-group">
					<label>Foto profil</label>
					<input type="file" name="photo" class="form-control">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" ng-model="inputData.email" name="email" class="form-control" required>
					<div class="error-container" ng-show="forms.registerForm.email.$error && forms.registerForm.email.$dirty" ng-messages="forms.registerForm.email.$error">
						<div ng-messages-include="error-list.html"></div>
					</div>
				</div>
				<div class="form-group">
					<label>Kata sandi</label>
					<input type="password" ng-model="inputData.password" name="password" class="form-control" required>
					<div class="error-container" ng-show="forms.registerForm.password.$error && forms.registerForm.password.$dirty" ng-messages="forms.registerForm.password.$error">
						<div ng-messages-include="error-list.html"></div>
					</div>
				</div>
				<div class="form-group">
					<label>Mohon ketikkan ulang kata sandi</label>
					<input type="password" ng-model="inputData.passwordConfirmation" name="password_confirmation" class="form-control" required>
					<div class="error" ng-show="inputData.password != inputData.passwordConfirmation">
						<i class="glyphicon glyphicon-exclamation-sign"></i> 
						Kata sandi tidak cocok
					</div>
					<div class="error-container" ng-show="forms.registerForm.password_confirmation.$error && forms.registerForm.password_confirmation.$dirty" ng-messages="forms.registerForm.password_confirmation.$error">
						<div ng-messages-include="error-list.html"></div>
					</div>
				</div>

				<div class="btn btn btn-primary" ng-click="saveRegister()" ng-disabled="buttonClicked"><span class="glyphicon glyphicon-user"></span> Daftar</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset("app/controllers/register.js") %>" type="text/javascript"></script>
@endsection