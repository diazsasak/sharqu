@extends('layout')
@section('title')
Selamat Datang di SHARQU
@endsection
@section('welcome')
<div class="jumbotron" style="background-image: url(<% asset('pictures/welcome2.jpg') %>); background-position: center; height: 600px;">
<div style="margin:60px;color:white;">
  <h1>Selamat datang di SHARQU !</h1>
  <p>Sebuah media sharing materi pembelajaran dan kuis secara ONLINE</p>
  <p><a class="btn btn-primary btn-lg" href="<% URL::to('login') %>" role="button"><span class="glyphicon glyphicon-hand-right"></span> Mulai berbagi dan belajar</a></p>
</div>
</div>
@endsection