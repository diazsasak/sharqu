@extends('layout')
@section('title')
Kuis
@endsection
@section('content')
<div class="row" ng-controller="QuizCtrl">
	<div class="col-md-8 col-md-offset-2">
		<div class="row">
			<div class="col-md-1">
				<div class="">No</div>
			</div>
			<div class="col-md-10 text-left">
				Pertanyaan :
			</div>
			<div class="col-md-1">
				Hapus
			</div>
		</div>
		<div ng-repeat="z in questions">
			<div class="row">
				<div class="col-md-1">
					<div class="">{{$index+1}}</div>
				</div>
				<div class="col-md-10">
					<input type="text" class="form-control" ng-model="z.content">
				</div>
				<div class="col-md-1">
					<button class="btn btn-danger" ng-click="delQuestion($index)"><span class="glyphicon glyphicon-trash"></span></button>
				</div>
			</div>
			<div class="row">
				<div class="text-left col-md-11 col-md-offset-1">
					Jawaban :
				</div>
			</div>
			<div class="row">
				<div class="col-md-1 col-md-offset-1">
					A.
				</div>
				<div class="col-md-3">
					<input type="text" ng-model="z.answer[0].content" class="form-control">
				</div>
				<div class="col-md-1">
					<input type="checkbox" ng-model="z.answer[0].key" ng-true-value="1" ng-false-value="0">
				</div>
				<div class="col-md-1">
					B.
				</div>
				<div class="col-md-3">
					<input type="text" ng-model="z.answer[1].content" class="form-control">
				</div>
				<div class="col-md-1">
					<input type="checkbox" ng-model="z.answer[1].key" ng-true-value="1" ng-false-value="0">
				</div>
			</div>
			<div class="row">
				<div class="col-md-1 col-md-offset-1">
					C.
				</div>
				<div class="col-md-3">
					<input type="text" ng-model="z.answer[2].content" class="form-control">
				</div>
				<div class="col-md-1">
					<input type="checkbox" ng-model="z.answer[2].key" ng-true-value="1" ng-false-value="0">
				</div>
				<div class="col-md-1">
					D.
				</div>
				<div class="col-md-3">
					<input type="text" ng-model="z.answer[3].content" class="form-control">
				</div>
				<div class="col-md-1">
					<input type="checkbox" ng-model="z.answer[3].key" ng-true-value="1" ng-false-value="0">
				</div>
			</div>
			<br/>
		</div>
		<div class="row">
			<div class="col-md-offset-1 text-left">
				<button class="btn btn-primary" ng-click="addQuestion()"><span class="glyphicon glyphicon-plus"></span></button>
			</div>
			<br/>
		</div>
		<div class="row">
			<button class="btn btn-success" ng-click="saveQuiz()"><span class="glyphicon glyphicon-save"></span> Simpan</button>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset("app/controllers/quiz.js") %>" type="text/javascript"></script>
@endsection