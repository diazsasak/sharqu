@extends('layout')
@section('title')
Kuis
@endsection
@section('content')
<div class="row" ng-controller="TakeQuizCtrl">
	<div class="col-md-8 col-md-offset-2">
		<div class="row">
			<div class="page-header text-left">
			<h1>Kuis {{postTitle}} <small class="label label-success">{{questionIndex+1}}/{{lastQuestion}}</small></h1>
			<small class="label label-success">Benar : {{point.correct}}</small>
			<small class="label label-danger">Salah : {{point.incorrect}}</small>
			<small class="label label-warning">Tak terjawab : {{point.miss}}</small>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success" role="alert">{{z.content}}</div>
			</div>
		</div>
		<div class="row text-left">
			<div class="col-md-5 col-md-offset-1">
				<div class="alert alert-info" role="alert" ng-click="checkAnswer(0)">A. {{z.answer[0].content}}</div>
			</div>
			<div class="col-md-5">
				<div class="alert alert-info" role="alert" ng-click="checkAnswer(1)">B. {{z.answer[1].content}}</div>
			</div>
		</div>
		<div class="row text-left">
			<div class="col-md-5 col-md-offset-1">
				<div class="alert alert-info" role="alert" ng-click="checkAnswer(2)">C. {{z.answer[2].content}}</div>
			</div>
			<div class="col-md-5">
				<div class="alert alert-info" role="alert" ng-click="checkAnswer(3)">D. {{z.answer[3].content}}</div>
			</div>
		</div>
		<div class="row">
			<br/>
			<div class="progress">
				<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="{{progressBarvalue}}" aria-valuemin="0" aria-valuemax="3" style="width: {{progressBarvalue*100/3}}%">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset("app/controllers/take_quiz.js") %>" type="text/javascript"></script>
@endsection