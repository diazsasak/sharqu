@extends('layout')
@section('title')
@if(isset($score[0]))
Tabel Skor Kuis <% $score[0]->title %>
@endif
@endsection
@section('content')
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		@if(isset($score[0]))
		<h1>Tabel Skor Kuis <% $score[0]->title %></h1>
		<div class="list-group">
			@foreach($score as $a)
			@if(Auth::user()->id == $a->user_id)
			<a href="#" class="list-group-item text-left active">
				@else
				<a href="#" class="list-group-item text-left">
					@endif
					<small class="label label-success" style="font-size: 20px;"><% $a->score %></small>
					<img src="<% asset($a->photo) %>" style="height: 40px; width: 30px;">
					<% $a->first_name %> <% $a->last_name %> 
				</a>
				@endforeach
			</div>
			@if($data['participated'])
			<a href="<% URL::to('quiz/'.$score[0]->id) %>"><button type="button" class="btn btn-lg btn-success" disabled="disabled"><span class="glyphicon glyphicon-hand-right"></span> Ikuti Kuis</button></a>
			@else
			<a href="<% URL::to('quiz/'.$score[0]->id) %>"><button type="button" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-hand-right"></span> Ikuti Kuis</button></a>
			@endif
			@else
			<h4 class="alert alert-info">Belum ada partisipan :)</h4>
			@if(isset($data['post_id']))
			<a href="<% URL::to('quiz/'.$data['post_id']) %>"><button type="button" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-hand-right"></span> Ikuti Kuis</button></a>
			@else
			<h4 class="alert alert-info">Belum ada soal kuis :)</h4>
			@endif
			@endif
		</div>
	</div>
	@endsection