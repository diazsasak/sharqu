@extends('layout')
@section('title')
Cari artikel
@endsection
@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-3">
			<h1 class="text-left">Kategori</h1>
			<ul class="list-group">
				@foreach($category as $a)
				<a href="<% URL::to('post/by_category/'.$a->id) %>">
					<li class="list-group-item">
						<span class="badge"><% $a->count %></span>
						<% $a->name %>
					</li>
				</a>
				@endforeach
			</ul>
		</div>
		<div class="col-md-9">
			<h1 class="text-left">Artikel</h1>
			@if(isset($post))
			<div class="list-group">
			@foreach($post as $a)
				<div class="list-group-item">
					<h4 class="list-group-item-heading text-left"><% $a->title %></h4>
					<p class="list-group-item-text text-left">
						<% strip_tags(substr($a->content, 0, 100)) %> ... <br/>
						oleh : <em><% $a->first_name %> <% $a->last_name %></em> | <% $a->created_at %> <br/>
						<a href="<% URL::to('post/'.$a->id) %>" class="btn btn-primary" title="Baca artikel"><span class="glyphicon glyphicon-eye-open"></span></a>
						<a class="btn btn-info" href="<% URL::to('score_table/'.$a->id) %>" title="Tabel Skor"><span class="glyphicon glyphicon-list-alt"></span></a>
					</p>
				</div>
			@endforeach
			</div>
			@endif
		</div>
	</div>
</div>
@endsection