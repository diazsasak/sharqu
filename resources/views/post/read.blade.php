@extends('layout')
@section('title')
Baca Artikel - <% $post[0]->title %>
@endsection
@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div style="text-align: left;">
			<h1><% $post[0]->title %></h1>
			<div class="col-md-10">
				<div class="row">
					<img src="<% asset($post[0]->photo) %>" style="height: 40px; width: 30px;">
					<% $post[0]->first_name %> <% $post[0]->last_name %> <br/> di post pada : <% $post[0]->created_at %>
				</div>
				<div class="row">
					<b>Kategori : </b> 
					<em>
						@foreach($post['category'] as $a)
						<% $a['name'] %>, 
						@endforeach
					</em>
				</div>
				<div class="row">
						<p style="text-align: justify;">
							<?php echo $post[0]->content; ?>
						</p>
				</div>
				<div class="row">
					@if($post['participated'])
					<a href="<% URL::to('quiz/'.$post[0]->id) %>"><button type="button" class="btn btn-lg btn-success" disabled="disabled"><span class="glyphicon glyphicon-hand-right"></span> Ikuti Kuis</button></a>
					@else
					<a href="<% URL::to('quiz/'.$post[0]->id) %>"><button type="button" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-hand-right"></span> Ikuti Kuis</button></a>
					@endif
					<a class="btn btn-primary" href="<% URL::to('score_table/'.$post[0]->id) %>" title="Tabel Skor"><span class="glyphicon glyphicon-list-alt"></span></a>
				</div>
			</div>
		</div>
	</div>
	@endsection