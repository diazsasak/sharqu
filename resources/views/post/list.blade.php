@extends('layout')
@section('title')
Daftar post saya
@endsection
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="text-right">
			<a href="<% URL::to('post/create') %>" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></a>
		</div>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Judul</th>
					<th>Manajemen Soal Kuis</th>
					<th>Peringkat</th>
					<th>Edit</th>
					<th>Hapus</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1; ?>
				@foreach($data as $a)
				<tr>
					<td>
						<% $no %>
					</td>
					<td>
						<% $a->title %>
					</td>
					<td>
						<a class="btn btn-primary" href="<% URL::to('post/'.$a->id.'/quiz') %>"><span class="glyphicon glyphicon-star"></span></a>
					</td>
					<td>
						<a class="btn btn-success" href="<% URL::to('score_table/'.$a->id) %>"><span class="glyphicon glyphicon-list-alt"></span></a>
					</td>
					<td>
						<a class="btn btn-info" href="<% URL::to('post/'.$a->id.'/edit') %>"><span class="glyphicon glyphicon-edit"></span></a>
					</td>
					<td>
						<form action="<% URL::to('post/'.$a->id) %>" method="POST">
							<% csrf_field() %>
							<input type="hidden" name="_method" value="DELETE">
							<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
						</form>
					</td>
				</tr>
				<?php $no++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection