@extends('layout')
@section('title')
Buat post
@endsection
@section('content')
<div class="row" ng-controller="NewPostCtrl">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-2 text-left">
			<h1>Kategori</h1>
			<div ng-repeat="a in categories">
				<input type="checkbox" name="category" ng-model="checkedCategory[a.id]" ng-change="setCategory(a.id)"> {{a.name}} <br/>
			</div>
			<input type="text" class="form-control" ng-model="newCategory.name"> <br/>
			<button class="btn btn-primary" ng-click="saveNewCategory()"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
		</div>
		<div class="col-md-8 text-left">
			<h1>Buat post baru</h1>
			<div class="form-group text-left">
				<label>Judul</label>
				<input type="text" name="title" ng-model="inputData.title" class="form-control" required>
			</div>
			<label>Konten</label>
			<textarea ui-tinymce="tinymceOptions" ng-model="inputData.content" name="content"></textarea>
			<br/>
			<button class="btn btn-success" ng-click="savePost()"><span class="glyphicon glyphicon-save"></span> Simpan</button>
		</div>
	</div>
</div>
{{inputData}}
</div>
@endsection
@section('script')
<script>
	// tinymce.init({
	// 	selector: '#mytextarea',
	// 	plugins: 'code'
	// });
</script>
<script src="<% asset("app/controllers/new_post.js") %>" type="text/javascript"></script>
@endsection