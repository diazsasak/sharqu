@extends('layout')
@section('title')
Edit post
@endsection
@section('content')
<div class="row" ng-controller="EditPostCtrl">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-2" style="text-align: left;">
			<h1>Kategori</h1>
			<div ng-repeat="a in categories">
				<input type="checkbox" name="category" ng-model="checkedCategory[a.id]" ng-change="setCategory(a.id)"> {{a.name}} <br/>
			</div>
			<input type="text" class="form-control" ng-model="newCategory.name"><br/>
			<button class="btn btn-primary" ng-click="saveNewCategory()"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
		</div>
		<div class="col-md-8 text-left">
			<h1>Edit post</h1>
			Dibuat pada : {{postInfo.post.created_at}} <br/>
			Terakhir dimodifikasi : {{postInfo.post.updated_at}}
			<div class="form-group text-left">
				<label>Judul</label>
				<input type="text" name="title" ng-model="inputData.title" class="form-control" required>
			</div>
			<label>Konten</label>
			<textarea ui-tinymce="tinymceOptions" ng-model="inputData.content" name="content"></textarea> <br/>
			<button class="btn btn-success" ng-click="savePost()"><span class="glyphicon glyphicon-save"></span> Simpan</button>
		</div>
	</div>
</div>
{{inputData}}
</div>
@endsection
@section('script')
<script src="<% asset("app/controllers/edit_post.js") %>" type="text/javascript"></script>
@endsection