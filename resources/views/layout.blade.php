<!DOCTYPE html>
<html ng-app="sharquApp">
<head>
<title>@yield('title') - SHARQU</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<%asset('bower_components/bootstrap/dist/css/bootstrap.min.css')%>" media="screen">
    <link rel="stylesheet" href="<%asset('bower_components/bootstrap/dist/css/bootstrap-theme.min.css')%>" media="screen">
    <link rel="stylesheet" href="<% asset('dist/css/navbar-color.css')%>" media="screen">
    <link href="<% asset("dist/css/form-validation.css") %>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<% asset('dist/css/custom.css') %>" media="screen">
    <!-- Angular Toastr -->
    <link href="<% asset("bower_components/angular-toastr/dist/angular-toastr.min.css") %>" rel="stylesheet" type="text/css" />
    @yield('head')
</head>
<body>
    <div class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynav">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">SHARQU</a>
            </div>
            <div class="collapse navbar-collapse" id="mynav">
                <ul class="nav navbar-nav">
                    @if(Auth::check())
                    <li><a href="<% URL::to('my_post') %>"><i class="glyphicon glyphicon-book" ></i> Post saya</a></li>
                    <li><a href="<% URL::to('post') %>"><i class="glyphicon glyphicon-search" ></i> Cari artikel</a></li>
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                    <li><img src="<% asset(Auth::user()->photo) %>" style="height: 40px; width: 30px; margin-top:5px;"></li>
                    <li><a><% Auth::user()->first_name %> <% Auth::user()->last_name %></a></li>
                    <li><a href="<% URL::to('logout') %>"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    @else
                    <li><a href="<% URL::to('register') %>"><span class="glyphicon glyphicon-user"></span> Daftar</a></li>
                    <li><a href="<% URL::to('login') %>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>   
                    @endif
                          
                </ul>
            </div>
        </div>
    </div>
    @yield('welcome')
    <div class="container-fluid" style="text-align: center;margin-top: 70px;margin-bottom: 70px;">
        <!-- if there are creation errors, they will show here -->
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <%-- pesan error --%>
                @if(count($errors) > 0)
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <ul>
                      @foreach($errors->all() as $error)
                      <li>{!! $error !!}</li>
                      @endforeach 
                  </ul>
              </div>
              @endif
              <%-- pesan sukses --%>
              @if(Session::has('success'))
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <% Session::pull('success') %>
            </div>
            @endif
            <%-- pesan gagal --%>
            @if(Session::has('fail'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <% Session::pull('fail') %>
            </div>
            @endif
        </div>
    </div>
    <script id="error-list.html" type="text/ng-template">  
      <div class="error" ng-message="required">
        <i class="glyphicon glyphicon-exclamation-sign"></i> 
        Harus diisi
    </div>
    <div class="error" ng-message="minlength">
        <i class="glyphicon glyphicon-exclamation-sign"></i> 
        Terlalu penek
    </div>
    <div class="error" ng-message="maxlength">
        <i class="glyphicon glyphicon-exclamation-sign"></i> 
        Terlalu panjang
    </div>
    <div class="error" ng-message="email">
        <i class="glyphicon glyphicon-exclamation-sign"></i> 
        Format email salah
    </div>
</script>
@yield('content')
</div>
<div class="navbar navbar-custom navbar-fixed-bottom text-center" style="color:white;padding-top:14px;">
    <b>SHARQU</b> - By Diaz Guntur Febrian for Lomba Web Design STTH Pancor
</div>
<script src="<% asset('bower_components/jquery/dist/jquery.min.js')%>"></script>
<script src="<% asset('bower_components/bootstrap/dist/js/bootstrap.min.js')%>"></script>
<script src="<% asset('bower_components/tinymce-dist/tinymce.min.js') %>"></script>
<script src="<% asset('bower_components/angular/angular.min.js') %>"></script>
<script src="<% asset('bower_components/angular-ui-tinymce/src/tinymce.js') %>"></script>
<script src="<% asset('bower_components/angular-messages/angular-messages.min.js') %>" type="text/javascript"></script>
<script src="<% asset('bower_components/angular-animate/angular-animate.min.js') %>" type="text/javascript"></script>
<script src="<% asset('bower_components/angular-toastr/dist/angular-toastr.tpls.min.js') %>" type="text/javascript"></script>
<script src="<% asset('app/app.js') %>" type="text/javascript"></script>
<script src="<% asset('app/controllers.js') %>" type="text/javascript"></script>
@yield('script')
</body>
</html>