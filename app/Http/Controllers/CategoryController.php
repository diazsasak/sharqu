<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

class CategoryController extends Controller
{
	public function index(Request $request){
		if($request->ajax()){
			return Category::all();
		}
	}

    public function store(Request $request){
    	Category::create($request->all());
    }
}
