<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Register;
use App\Http\Requests\Login;
use App\User;
use Image;
use Session;
use Auth;

class AuthController extends Controller
{
    public function register(){
        return view('auth.register');
    }
    
    public function postRegister(Register $request){
        //simpan data
        $input = $request->except('photo');        
        $input['password'] = bcrypt($input['password']);
        $register = User::create($input);

        //simpan foto
        $file = $request->file('photo');
        if($file != null){
            $photo = 'pictures/profile/user_'.$register->id.'.'.$file->getClientOriginalExtension();
            Image::make($file)->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($photo);
        }
        else{
            $photo = 'pictures/profile/default_profile_picture.png';
        }
        $register->photo = $photo;
        $register->save();

        Session::set('success', 'Pendaftaran berhasil, silahkan masuk');
        return view('auth.login');
    }

    public function postLogin(Login $request){
        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember'))){
                Session::set('success', 'Login berhasil');
                return redirect('/');
        }
        else{
            Session::set('fail', 'Email atau password anda salah');
            return redirect()->route('login');
        }
    }

    public function login(){
        if(Auth::check()){
            return redirect('/');
        }
        else{
            return view('auth.login');
        }
    }

    public function logout(){
        Auth::logout();
        Session::set('success', 'Anda telah logout');
        return redirect('/');
    }
}
