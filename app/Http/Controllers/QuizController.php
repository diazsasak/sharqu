<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Question;
use App\Answer;
use App\Post;
use App\Participant;
use Auth;
use DB;

class QuizController extends Controller
{
    public function index(Request $request, $id){
    	if($request->ajax()){
    		$data = Question::where('post_id', $id)->get();
    		if($data->isEmpty()){
    			return 'zero';
    		}
    		else{
    			$question = Question::where('post_id', $id)->get(['id','content', 'post_id']);
    			for($i = 0; $i < sizeOf($question); $i++){
    				$question_id = $question[$i]['id'];
    				$answer = Answer::where('question_id', '=', $question_id)->get(['content', 'key']);
    				$question[$i]['answer'] = $answer;
    			}
    			return $question;
    		}
    	}
    	else{
    		return view('quiz.index');
    	}
    }

    public function store(Request $request){
    	$input = $request->all();
    	Question::where('post_id', '=', $input[0]['post_id'])->delete();
    	for($i = 0; $i < sizeOf($input); $i++){
    		$question['content'] = $input[$i]['content'];
    		$question['post_id'] = $input[$i]['post_id'];
    		$q = Question::create($question);

    		for($j = 0; $j < sizeof($input[$i]['answer']); $j++){
    			$answer['question_id'] = $q->id;
    			$answer['content'] = $input[$i]['answer'][$j]['content'];
    			$answer['key'] = $input[$i]['answer'][$j]['key'];
    			Answer::create($answer);
    		}
    	}
    }

    public function saveScore(Request $request){
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        Participant::create($input);
    }

    public function show(Request $request, $id){
        $quizStatus = Participant::where('post_id', $id)->where('user_id', Auth::user()->id)->get();
        $question = Question::where('post_id', $id)->get();
        if($quizStatus->isEmpty() && !$question->isEmpty()){
            if($request->ajax()){
                $data = Question::where('post_id', $id)->get();
                if($data->isEmpty()){
                    return 'zero';
                }
                else{
                    $question = Question::where('post_id', $id)->get(['id','content', 'post_id']);
                    for($i = 0; $i < sizeOf($question); $i++){
                        $question_id = $question[$i]['id'];
                        $answer = Answer::where('question_id', '=', $question_id)->get(['content', 'key']);
                        $question[$i]['answer'] = $answer;
                    }
                    $postTitle = Post::where('id', $id)->get(['title']);
                    $data = [
                    'post_title' => $postTitle,
                    'question' => $question
                    ];
                    return $data;
                }
            }
            else{
                return view('quiz.take');
            }
        }
    }

    public function scoreTable($id){
        $score = DB::select(DB::raw("
            SELECT participant.`score`, users.`first_name`, users.id as user_id,
            users.`last_name`, users.`photo`, post.`title`, post.id
            FROM participant, users, post
            WHERE
            participant.`post_id` = $id AND
            participant.`user_id` = users.`id` AND
            participant.`post_id` = post.`id`
            ORDER BY participant.score DESC
            "));
        $quizStatus = Participant::where('post_id', $id)->where('user_id', Auth::user()->id)->get();
        $question = Question::where('post_id', $id)->get();
        if($quizStatus->isEmpty() && !$question->isEmpty()){
            $data['participated'] = false;
            $data['post_id'] = $id;
        }
        else{
            $data['participated'] = true;
        }
        return view('quiz.score_table')->with('score', $score)->with('data', $data);
    }
}
