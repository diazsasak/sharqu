<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\PostForm;
use App\Post;
use App\PostCategory;
use App\Category;
use Auth;
use DB;
use App\User;
use App\Participant;
use App\Question;

class PostController extends Controller
{
    public function index(){
        $category = DB::select(DB::raw("
            SELECT category.name, category.id, COUNT(*) as count
            FROM category, post_category
            WHERE
            category.`id` = post_category.`category_id`
            GROUP BY category.`id`
            "));

        return view('post.search')->with('category', $category);
    }

    public function getByCategory($id){
        $category = DB::select(DB::raw("
            SELECT category.name, category.id, COUNT(*) as count
            FROM category, post_category
            WHERE
            category.`id` = post_category.`category_id`
            GROUP BY category.`id`
            "));
        $post = DB::select(DB::raw("
            SELECT post.*, users.`first_name`, users.`last_name`
            FROM post, users 
            WHERE 
            post.`writer_id` = users.`id` AND
            post.id
            IN (
            SELECT post_id FROM post_category WHERE category_id = $id
            )
            "));

        return view('post.search')->with('category', $category)->with('post', $post);
    }
    public function create(){
        return view('post.create');
    }

    public function myPost(){
        $data = Post::where('writer_id','=', Auth::user()->id)->get();
        return view('post.list')->with('data', $data);
    }

    public function edit(Request $request, $id){
        if($request->ajax()){
            $post = Post::where('writer_id','=', Auth::user()->id)
            ->where('id', '=', $id)
            ->get();
            $category = PostCategory::where('post_id', '=', $id)->get(['category_id']);
            $data = [
            'post' => $post[0],
            'category' => $category
            ];
            return $data;
        }
        else{
            return view('post.edit');
        }
    }

    public function store(PostForm $request){
        $post = $request->except('category');
        $post['writer_id'] = Auth::user()->id;
        $post = Post::create($post);

        $category = $request->input('category');
        $data['post_id'] = $post->id;
        for($i = 0; $i < sizeof($category); $i++) {
            $data['category_id'] = $category[$i];
            PostCategory::create($data);
        }
    }

    public function show($id){
        $post = DB::select(DB::raw("
            SELECT post.*, users.`first_name`, users.`last_name`, users.`photo`
            FROM post, users
            WHERE 
            post.`id` = $id AND
            post.`writer_id` = users.`id`
            "));
        $category = DB::select(DB::raw("
            SELECT category.`name`
            FROM category, post_category
            WHERE
            post_category.`post_id` = $id AND
            category.`id` = post_category.`category_id`
            "));

        $quizStatus = Participant::where('post_id', $id)->where('user_id', Auth::user()->id)->get();
        $question = Question::where('post_id', $id)->get();
        if($quizStatus->isEmpty() && !$question->isEmpty()){
            $post['participated'] = false;
        }
        else{
            $post['participated'] = true;
        }
        // $post = json_decode(json_encode($post), true);
        $category = json_decode(json_encode($category), true);
        // $post = $post[0];
        $post['category'] = $category;
        // var_dump($post);
        return view('post.read')->with('post', $post);
    }

    public function update(PostForm $request, $id){
        $post = $request->except('category');
        $post = Post::where('writer_id','=', Auth::user()->id)
        ->where('id', '=', $id)
        ->update($post);

        PostCategory::where('post_id', '=', $id)->delete();
        $category = $request->input('category');
        $data['post_id'] = $id;
        for($i = 0; $i < sizeof($category); $i++) {
            $data['category_id'] = $category[$i];
            PostCategory::create($data);
        }
    }

    public function quiz(Request $request, $id){
        return view('quiz.index');
    }

    public function destroy($id){
        Post::find($id)->delete();
        return redirect('/my_post');
    }
}
