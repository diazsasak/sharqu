<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Blade::setContentTags('<%', '%>');
Blade::setEscapedContentTags('<!!', '!!>');



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	Route::get('/', function () {
		return view('welcome');
	});
	//Register
	Route::get('register', [
		'as' => 'register', 'uses' => 'AuthController@register'
		]);
	Route::post('register', [
		'as' => 'post.register', 'uses' => 'AuthController@postRegister'
		]);
	//login
	Route::get('login', [
		'as' => 'login', 'uses' => 'AuthController@login'
		]);
	Route::post('login', [
		'as' => 'post.login', 'uses' => 'AuthController@postLogin'
		]);

	Route::group(['middleware' => ['auth']], function () {
		Route::get('my_post', 'PostController@myPost');
		Route::get('post/by_category/{id}', 'PostController@getByCategory');
		Route::get('post/{id}/quiz', 'QuizController@index');
		Route::resource('post', 'PostController');
		Route::resource('category', 'CategoryController');
		Route::post('quiz/save_score', 'QuizController@saveScore');
		Route::resource('quiz', 'QuizController');
		Route::get('score_table/{id}', 'QuizController@scoreTable');

		Route::get('logout', [
		'as' => 'logout', 'uses' => 'AuthController@logout'
		]);
	});
});
