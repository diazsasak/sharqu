angular.module('sharquApp.controllers')
.controller('TakeQuizCtrl', ['$scope', '$http', '$window', '$interval', 'toastr',
	function($scope, $http, $window, $interval, toastr){
		var pathArray = window.location.pathname.split( '/' );
		$scope.questionIndex = 0;
		$scope.progressBarvalue = 3;
		$scope.point = {
			correct: 0,
			incorrect: 0,
			miss: 0,
			final_score: 0
		}
		$scope.getQuestion = function(){
			$http({
				url: '/quiz/'+pathArray[2],
				method: 'GET'
			}).success(function(data){
				$scope.postTitle = data.post_title[0].title;
				$scope.questions = data.question;
				$scope.lastQuestion = $scope.questions.length;
				$scope.startQuiz();
			});
		}
		$scope.changeQuestion = function(){
			if(($scope.questionIndex+1) == $scope.lastQuestion){
				$scope.stopQuiz();
			}
			else if(($scope.questionIndex+1) < $scope.lastQuestion){
				$scope.questionIndex++;
				$scope.z = $scope.questions[$scope.questionIndex];
				$scope.progressBarvalue = 3;
			}
		}
		$scope.startQuiz = function(){
			$scope.z = $scope.questions[$scope.questionIndex];
			$scope.stop = $interval(function(){
				if($scope.progressBarvalue > 0){
					$scope.progressBarvalue -= 1;
				}
				else{
					toastr.warning('Oops, waktu habis');
					$scope.point.miss++;
					$scope.changeQuestion();
				}
			}, 1000);
		}
		$scope.stopQuiz = function(){
			if (angular.isDefined($scope.stop)) {
				$interval.cancel($scope.stop);
				$scope.stop = undefined;
			}
			$scope.point.final_score = ($scope.point.correct*100) / $scope.lastQuestion;
			toastr.success('Point anda : '+$scope.point.final_score);
			$scope.saveScore();
		};
		$scope.saveScore = function(){
			$scope.inputData = {
				post_id: pathArray[2],
				score: $scope.point.final_score
			};
			$http({
				url: '/quiz/save_score',
				method: 'POST',
				data: $scope.inputData
			}).success(function(data){
				var landingUrl = "http://" + $window.location.host + "/score_table/"+pathArray[2];
				$window.location.href = landingUrl;
			});
		}
		$scope.checkAnswer = function(id){
			if($scope.questions[$scope.questionIndex].answer[id].key == 1){
				toastr.success('Jawaban anda benar');
				$scope.point.correct++;
				$scope.changeQuestion();
			}
			else if($scope.questions[$scope.questionIndex].answer[id].key == 0){
				toastr.error('Oops, salah');
				$scope.point.incorrect++;
				$scope.changeQuestion();
			}
		}
		$scope.getQuestion();
	}]);