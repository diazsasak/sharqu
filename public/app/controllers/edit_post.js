angular.module('sharquApp.controllers')
.controller('EditPostCtrl', ['$http', '$scope', 'toastr', '$window',
	function($http, $scope, toastr, $window){
		$scope.inputData = {
			category: [],
			title: '',
			content: ''
		}
		$scope.checkedCategory = [];
		$scope.tinymceOptions = {
			onChange: function(e) {
		      // put logic here for keypress and cut/paste changes
		  },
		  inline: false,
		  plugins : 'code',
		  skin: 'lightgray',
		  theme : 'modern'
		};
		$scope.checkSelectedCategory = function(){
			for(var j = 0; j < $scope.categories.length; j++){
				for(var i = 0; i < $scope.postInfo.category.length; i++){
					if($scope.categories[j].id == $scope.postInfo.category[i].category_id){
						$scope.checkedCategory[$scope.categories[j].id] = true;
						$scope.inputData.category.push($scope.categories[j].id);
					}
				}
			}
		}
		$scope.getCategories = function(){
			$http({
				url: '/category',
				method: 'GET'
			}).success(function(data){
				$scope.categories = data;
				$scope.checkSelectedCategory();
			});
		};
		$scope.saveNewCategory = function(){
			if($scope.newCategory.name != ''){
				$http({
					url: '/category',
					method: 'POST',
					data: $scope.newCategory
				}).success(function(){
					$scope.getCategories();
					$scope.newCategory.name = '';
				});
			}
		};
		$scope.setCategory = function(id){
			if ($scope.checkedCategory[id]) {
				$scope.inputData.category.push(id);
			} else {
				var _index = $scope.inputData.category.indexOf(id);
				$scope.inputData.category.splice(_index, 1);
			}
		}
		$scope.savePost = function(){
			$http({
				url: '/post/'+$scope.postInfo.post.id,
				method: 'PUT',
				data: $scope.inputData
			}).success(function(){
				var landingUrl = "http://" + $window.location.host + "/my_post";
				$window.location.href = landingUrl;
			}).error(function(message){
				for (var property in message) {
					if (message.hasOwnProperty(property)) {
						toastr.error(message[property]);
					}
				}
			});
		}

		var pathArray = window.location.pathname.split( '/' );
		$scope.getPost = function(){
			$http({
				url: '/post/'+pathArray[2]+'/edit',
				method: 'GET'
			}).success(function(data){
				console.log(data);
				$scope.inputData.title = data.post.title;
				$scope.inputData.content = data.post.content;
				$scope.postInfo = data;
				$scope.getCategories();
			});
		};
		$scope.getPost();
	}])