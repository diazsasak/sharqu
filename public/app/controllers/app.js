angular.module('sharquApp.controllers')

//controller untuk registrasi dan login
.controller('AppCtrl', ['$rootScope',  'UserFactory','Loader', '$state',
  function($rootScope, UserFactory, Loader, $state){
    $rootScope.logout = function(){
      UserFactory.logout();
      $rootScope.isAuthenticated = false;
      Loader.toggleLoadingWithMessage('Logout berhasil');
      $state.go('app.login');
    }
}]);