angular.module('sharquApp.controllers')
.controller('RegisterCtrl', ['$http', '$scope',
	function($http, $scope){
		$scope.forms = {};
		$scope.buttonClicked = false;
		$scope.inputData = {};
		$scope.saveRegister = function(){
			console.log($scope.forms.registerForm);
			$scope.forms.registerForm.first_name.$dirty = true;
			$scope.forms.registerForm.last_name.$dirty = true;
			$scope.forms.registerForm.email.$dirty = true;
			$scope.forms.registerForm.password.$dirty = true;
			$scope.forms.registerForm.password_confirmation.$dirty = true;
			if($scope.forms.registerForm.$valid){
				document.getElementById('registerForm').submit();
				$scope.buttonClicked = true;
			}
		}
	}])