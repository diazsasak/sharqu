angular.module('sharquApp.controllers')
.controller('NewPostCtrl', ['$http', '$scope', 'toastr', '$window',
	function($http, $scope, toastr, $window){
		$scope.inputData = {
			category: [],
			title: '',
			content: ''
		}
		$scope.checkedCategory = [];
		$scope.tinymceOptions = {
			onChange: function(e) {
		      // put logic here for keypress and cut/paste changes
		  },
		  inline: false,
		  plugins : 'code',
		  skin: 'lightgray',
		  theme : 'modern'
		};

		$scope.getCategories = function(){
			$http({
				url: '/category',
				method: 'GET'
			}).success(function(data){
				$scope.categories = data;
			});
		};
		$scope.saveNewCategory = function(){
			if($scope.newCategory.name != ''){
				$http({
					url: '/category',
					method: 'POST',
					data: $scope.newCategory
				}).success(function(){
					$scope.getCategories();
					$scope.newCategory.name = '';
				});
			}
		};
		$scope.setCategory = function(id){
			if ($scope.checkedCategory[id]) {
				$scope.inputData.category.push(id);
			} else {
				var _index = $scope.inputData.category.indexOf(id);
				$scope.inputData.category.splice(_index, 1);
			}
		}
		$scope.savePost = function(){
			$http({
				url: '/post',
				method: 'POST',
				data: $scope.inputData
			}).success(function(){
				var landingUrl = "http://" + $window.location.host + "/my_post";
				$window.location.href = landingUrl;
			}).error(function(message){
				for (var property in message) {
					if (message.hasOwnProperty(property)) {
				        toastr.error(message[property]);
				    }
				}
			});
		}
		$scope.getCategories();
	}])