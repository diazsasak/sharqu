angular.module('sharquApp.controllers')
.controller('QuizCtrl', ['$scope', '$http', '$window',
	function($scope, $http, $window){
		var pathArray = window.location.pathname.split( '/' );
		$scope.question = {
			post_id: pathArray[2],
			content: '',
			answer: [
			{
				content: '',
				key: 0
			},
			{
				content: '',
				key: 0
			},
			{
				content: '',
				key: 0
			},
			{
				content: '',
				key: 0
			}
			]
		};
		$scope.getQuestion = function(){
			$http({
				url: '/post/'+pathArray[2]+'/quiz',
				method: 'GET'
			}).success(function(data){
				if(data != null){
					$scope.questions = data;
				}
				if(data == 'zero'){
					$scope.questions = [
						$scope.question
					];
				}
			});
		}
		$scope.getQuestion();
		$scope.addQuestion = function(){
			$scope.questions.push(
			{
				post_id: pathArray[2],
				content: '',
				answer: [
				{
					content: '',
					key: 0
				},
				{
					content: '',
					key: 0
				},
				{
					content: '',
					key: 0
				},
				{
					content: '',
					key: 0
				}
				]
			}
			);
		}
		$scope.saveQuiz = function(){
			$http({
				url: '/quiz',
				method: 'POST',
				data: $scope.questions
			}).success(function(data){
					var landingUrl = "http://" + $window.location.host + "/my_post";
					$window.location.href = landingUrl;
				});
		}
		$scope.delQuestion = function(index){
			$scope.questions.splice(index, index);
		}
	}]);