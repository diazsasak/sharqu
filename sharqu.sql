/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.5-10.1.9-MariaDB : Database - sharqu
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sharqu` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sharqu`;

/*Table structure for table `answer` */

DROP TABLE IF EXISTS `answer`;

CREATE TABLE `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `key` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answer_question_id_foreign` (`question_id`),
  CONSTRAINT `answer_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `answer` */

insert  into `answer`(`id`,`content`,`question_id`,`key`,`created_at`,`updated_at`) values (1,'Hypertext Markup Language',1,1,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(2,'Hypertext Markup League',1,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(3,'Hypertext Markdown Language',1,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(4,'Hypertext Transfer Language',1,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(5,'4',2,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(6,'5',2,1,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(7,'3',2,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(8,'8',2,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(9,'src',3,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(10,'image',3,0,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(11,'picture',3,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(12,'img',3,1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(13,'div',4,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(14,'script',4,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(15,'link',4,1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(16,'td',4,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(17,'membuat kolom pada tabel',5,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(18,'membuat heading',5,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(19,'membuat tulisan tebal',5,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(20,'membuat baris pada tabel',5,1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(21,'unordered list',6,1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(22,'onperiode list',6,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(23,'uncentered list',6,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(24,'unammed list',6,0,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(25,'Document Object Model',7,1,'2016-03-25 06:34:18','2016-03-25 06:34:18'),(26,'Document Oriented Model',7,0,'2016-03-25 06:34:18','2016-03-25 06:34:18'),(27,'Document Object Markup',7,0,'2016-03-25 06:34:18','2016-03-25 06:34:18'),(28,'Data Object Model',7,0,'2016-03-25 06:34:18','2016-03-25 06:34:18'),(29,'Open System Interconnection',8,1,'2016-03-25 06:39:02','2016-03-25 06:39:02'),(30,'Open Standard Interconnection',8,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(31,'Open System Internet',8,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(32,'Opensource System Interconnection',8,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(33,'Switch',9,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(34,'Hub',9,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(35,'Bridge',9,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(36,'Router',9,1,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(37,'Transmission Control Port',10,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(38,'Transmission Central Protocol',10,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(39,'Transmission Control Protocol',10,1,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(40,'Trasnfer Control Protocol',10,0,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(41,'4',11,0,'2016-03-25 06:39:04','2016-03-25 06:39:04'),(42,'5',11,0,'2016-03-25 06:39:04','2016-03-25 06:39:04'),(43,'6',11,0,'2016-03-25 06:39:04','2016-03-25 06:39:04'),(44,'7',11,1,'2016-03-25 06:39:04','2016-03-25 06:39:04');

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`created_at`,`updated_at`) values (1,'web','2016-03-25 06:22:27','2016-03-25 06:22:27'),(2,'pemrograman','2016-03-25 06:22:33','2016-03-25 06:22:33'),(3,'jaringan komputer','2016-03-25 06:35:18','2016-03-25 06:35:18');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2016_03_04_153000_category',1),('2016_03_04_153010_post',1),('2016_03_04_153430_question',1),('2016_03_04_153445_answer',1),('2016_03_04_153556_participant',1),('2016_03_19_062310_post_category',1);

/*Table structure for table `participant` */

DROP TABLE IF EXISTS `participant`;

CREATE TABLE `participant` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `score` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `participant_user_id_foreign` (`user_id`),
  KEY `participant_post_id_foreign` (`post_id`),
  CONSTRAINT `participant_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `participant_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `participant` */

insert  into `participant`(`id`,`user_id`,`post_id`,`score`,`created_at`,`updated_at`) values (1,2,1,42.857142857143,'2016-03-25 06:42:04','2016-03-25 06:42:04'),(2,2,2,50,'2016-03-25 06:48:38','2016-03-25 06:48:38'),(3,3,1,42.857142857143,'2016-03-25 06:50:46','2016-03-25 06:50:46'),(4,3,2,0,'2016-03-25 06:51:29','2016-03-25 06:51:29'),(5,4,1,14.285714285714,'2016-03-25 06:52:33','2016-03-25 06:52:33'),(6,4,2,25,'2016-03-25 06:53:03','2016-03-25 06:53:03');

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `writer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_writer_id_foreign` (`writer_id`),
  CONSTRAINT `post_writer_id_foreign` FOREIGN KEY (`writer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `post` */

insert  into `post`(`id`,`title`,`content`,`writer_id`,`created_at`,`updated_at`) values (1,'Hypertext Markup Language ( HTML )','<p><strong style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">Hyper Text Markup Language</strong><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;(HTML) adalah sebuah&nbsp;</span><em style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Bahasa markah\" href=\"https://id.wikipedia.org/wiki/Bahasa_markah\">bahasa markah</a></em><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;yang digunakan untuk membuat sebuah halaman web, menampilkan berbagai informasi di dalam sebuah&nbsp;</span><a style=\"text-decoration: none; color: #0b0080; font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Penjelajah web\" href=\"https://id.wikipedia.org/wiki/Penjelajah_web\">penjelajah web</a><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;Internet dan pemformatan hiperteks sederhana yang ditulis dalam berkas format ASCII agar dapat menghasilkan tampilan wujud yang terintegerasi. Dengan kata lain, berkas yang dibuat dalam perangkat lunak pengolah kata dan disimpan dalam format&nbsp;</span><a style=\"text-decoration: none; color: #0b0080; font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"ASCII\" href=\"https://id.wikipedia.org/wiki/ASCII\">ASCII</a><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;normal sehingga menjadi&nbsp;</span><a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Halaman web\" href=\"https://id.wikipedia.org/wiki/Halaman_web\">halaman web</a><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;dengan perintah-perintah HTML. Bermula dari sebuah bahasa yang sebelumnya banyak digunakan di dunia penerbitan dan percetakan yang disebut dengan</span><a style=\"text-decoration: none; color: #0b0080; font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"SGML\" href=\"https://id.wikipedia.org/wiki/SGML\">SGML</a><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;(</span><em style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">Standard Generalized Markup Language</em><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">), HTML adalah sebuah standar yang digunakan secara luas untuk menampilkan halaman web. HTML saat ini merupakan standar&nbsp;</span><a style=\"text-decoration: none; color: #0b0080; font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Internet\" href=\"https://id.wikipedia.org/wiki/Internet\">Internet</a><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;yang didefinisikan dan dikendalikan penggunaannya oleh&nbsp;</span><a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; font-family: sans-serif; font-size: 14px; line-height: 22.4px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"World Wide Web Consortium\" href=\"https://id.wikipedia.org/wiki/World_Wide_Web_Consortium\">World Wide Web Consortium</a><span style=\"color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;(W3C). HTML dibuat oleh kolaborasi Caillau TIM dengan Berners-lee Robert ketika mereka bekerja di CERN pada tahun 1989 (CERN adalah lembaga penelitian fisika energi tinggi di Jenewa).<br /><br /></span></p>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\">Pada tahun 1980 seorang ahli fisika,&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Tim Berners-Lee\" href=\"https://id.wikipedia.org/wiki/Tim_Berners-Lee\">Tim Berners-Lee</a>, dan juga seorang kontraktor di CERN (Organisasi Eropa untuk Riset Nuklir) mengusulkan dan menyusun ENQUIRE, sebuah sistem untuk ilmuwan CERN dalam membagi dokumen. Sembilan tahun kemudian, Berners-Lee mengusulkan adanya sistem markah berbasis internet.<sup id=\"cite_ref-2\" class=\"reference\" style=\"line-height: 1em; unicode-bidi: isolate;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" href=\"https://id.wikipedia.org/wiki/HTML#cite_note-2\">[2]</a></sup>&nbsp;Berners-Lee menspesifikasikan HTML dan menulis jaringan beserta perangkat lunaknya di akhir 1990. Di tahun yang sama, Berners-Lee dan&nbsp;<a class=\"new\" style=\"text-decoration: none; color: #a55858; background: none;\" title=\"Robert Cailliau (halaman belum tersedia)\" href=\"https://id.wikipedia.org/w/index.php?title=Robert_Cailliau&amp;action=edit&amp;redlink=1\">Robert Cailliau</a>, insinyur sistem data CERN berkolaborasi dalam sebuah permintaan untuk pendanaan, namun tidak diterima secara resmi oleh CERN. Di catatan pribadinya<sup id=\"cite_ref-3\" class=\"reference\" style=\"line-height: 1em; unicode-bidi: isolate;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" href=\"https://id.wikipedia.org/wiki/HTML#cite_note-3\">[3]</a></sup>&nbsp;sejak 1990 dia mendaftar<sup id=\"cite_ref-4\" class=\"reference\" style=\"line-height: 1em; unicode-bidi: isolate;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" href=\"https://id.wikipedia.org/wiki/HTML#cite_note-4\">[4]</a></sup>&nbsp;\"beberapa dari banyak daerah yang menggunakan hypertext\" dan pertama-tama menempatkan sebuah ensiklopedia.</p>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\">Penjelasan pertama yang dibagi untuk umum dari HTML adalah sebuah dokumen yang disebut \"Tanda HTML\", pertama kali disebutkan di Internet oleh Tim Berners-Lee pada akhir 1991.<sup id=\"cite_ref-tagshtml_5-0\" class=\"reference\" style=\"line-height: 1em; unicode-bidi: isolate;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" href=\"https://id.wikipedia.org/wiki/HTML#cite_note-tagshtml-5\">[5]</a></sup><sup id=\"cite_ref-6\" class=\"reference\" style=\"line-height: 1em; unicode-bidi: isolate;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" href=\"https://id.wikipedia.org/wiki/HTML#cite_note-6\">[6]</a></sup>&nbsp;Tanda ini menggambarkan 18 elemen awal mula, versi sederhana dari HTML. Kecuali untuk&nbsp;<em>tag hyperlink</em>, yang sangat dipengaruhi oleh SGMLguid, in-house Standard Generalized Markup Language (SGML) berbasis format dokumen di CERN. Sebelas elemen ini masih ada di HTML 4.<sup id=\"cite_ref-7\" class=\"reference\" style=\"line-height: 1em; unicode-bidi: isolate;\"><a style=\"text-decoration: none; color: #0b0080; background: none;\" href=\"https://id.wikipedia.org/wiki/HTML#cite_note-7\">[7]</a></sup></p>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\">HTML adalah bahasa markah yang digunakan peramban untuk menafsirkan dan menulis teks, gambar dan bahan lainnya ke dalam halaman web secara visual maupun suara. Karakteristik dasar untuk setiap item dari markah HTML didefinisikan di dalam peramban, dan karakteristik ini dapat diubah atau ditingkatkan dengan menggunakan tambahan halaman web desainer&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"CSS\" href=\"https://id.wikipedia.org/wiki/CSS\">CSS</a>. Banyak elemen teks ditemukan di laporan teknis ISO pada tahun 1988 TR 9537&nbsp;<em>Teknik untuk menggunakan SGML</em>, yang pada gilirannya meliputi fitur bahasa format teks awal seperti yang digunakan oleh komandan&nbsp;<em>RUNOFF</em>&nbsp;dikembangkan pada awal 1960-an untuk sistem operasi: perintah-perintah format ini berasal dari perintah yang digunakan oleh pengetik untuk memformat dokumen CTSS secara manual. Namun, konsep SGML dari markah umum didasarkan pada unsur-unsur daripada hanya efek cetak, dengan pemisahan struktur dan markah juga; HTML telah semakin bergerak ke arah ini dengan CSS.</p>',1,'2016-03-25 06:24:26','2016-03-25 06:24:26'),(2,'OSI Layer','<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\"><strong>Model referensi jaringan terbuka OSI</strong>&nbsp;atau&nbsp;<em><strong>OSI Reference Model for open networking</strong></em>&nbsp;adalah sebuah model arsitektural jaringan yang dikembangkan oleh badan&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"International Organization for Standardization\" href=\"https://id.wikipedia.org/wiki/International_Organization_for_Standardization\">International Organization for Standardization</a>&nbsp;(ISO) di&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Eropa\" href=\"https://id.wikipedia.org/wiki/Eropa\">Eropa</a>&nbsp;pada tahun&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"1977\" href=\"https://id.wikipedia.org/wiki/1977\">1977</a>. OSI sendiri merupakan singkatan dari&nbsp;<em><strong>Open System Interconnection</strong></em>. Model ini disebut juga dengan model \"<strong>Model tujuh lapis OSI</strong>\" (<em>OSI seven layer model</em>).</p>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\">Sebelum munculnya model referensi OSI, sistem jaringan komputer sangat tergantung kepada pemasok (<em>vendor</em>). OSI berupaya membentuk standar umum jaringan komputer untuk menunjang interoperatibilitas antar pemasok yang berbeda. Dalam suatu jaringan yang besar biasanya terdapat banyak&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Protokol jaringan\" href=\"https://id.wikipedia.org/wiki/Protokol_jaringan\">protokol jaringan</a>&nbsp;yang berbeda. Tidak adanya suatu protokol yang sama, membuat banyak perangkat tidak bisa saling berkomunikasi.</p>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\">Model referensi ini pada awalnya ditujukan sebagai basis untuk mengembangkan&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Protokol jaringan\" href=\"https://id.wikipedia.org/wiki/Protokol_jaringan\">protokol-protokol jaringan</a>, meski pada kenyataannya inisatif ini mengalami kegagalan. Kegagalan itu disebabkan oleh beberapa faktor berikut:</p>\n<ul style=\"margin: 0.3em 0px 0px 1.6em; padding: 0px; list-style-image: url(\'//upload.wikimedia.org/wikipedia/en/1/18/Monobook-bullet.png\'); color: #252525; font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">\n<li style=\"margin-bottom: 0.1em;\">Standar model referensi ini, jika dibandingkan dengan&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"DARPA Reference Model\" href=\"https://id.wikipedia.org/wiki/DARPA_Reference_Model\">model referensi DARPA (Model Internet)</a>&nbsp;yang dikembangkan oleh&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Internet Engineering Task Force\" href=\"https://id.wikipedia.org/wiki/Internet_Engineering_Task_Force\">Internet Engineering Task Force (IETF)</a>, sangat berdekatan. Model DARPA adalah model basis protokol TCP/IP yang populer digunakan.</li>\n<li style=\"margin-bottom: 0.1em;\">Model referensi ini dianggap sangat kompleks. Beberapa fungsi (seperti halnya metode komunikasi connectionless) dianggap kurang bagus, sementara fungsi lainnya (seperti<em>flow control</em>&nbsp;dan koreksi kesalahan) diulang-ulang pada beberapa lapisan.</li>\n<li style=\"margin-bottom: 0.1em;\">Pertumbuhan&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Internet\" href=\"https://id.wikipedia.org/wiki/Internet\">Internet</a>&nbsp;dan protokol&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"TCP/IP\" href=\"https://id.wikipedia.org/wiki/TCP/IP\">TCP/IP</a>&nbsp;(sebuah protokol jaringan dunia nyata) membuat OSI Reference Model menjadi kurang diminati.</li>\n</ul>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\">Pemerintah&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Amerika Serikat\" href=\"https://id.wikipedia.org/wiki/Amerika_Serikat\">Amerika Serikat</a>&nbsp;mencoba untuk mendukung protokol OSI Reference Model dalam solusi jaringan pemerintah pada tahun 1980-an, dengan mengimplementasikan beberapa standar yang disebut dengan&nbsp;<em><strong>Government Open Systems Interconnection Profile</strong></em>&nbsp;(GOSIP). Meski demikian. usaha ini akhirnya ditinggalkan pada tahun&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"1995\" href=\"https://id.wikipedia.org/wiki/1995\">1995</a>, dan implementasi jaringan yang menggunakan&nbsp;<em>OSI Reference model</em>&nbsp;jarang dijumpai di luar&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Eropa\" href=\"https://id.wikipedia.org/wiki/Eropa\">Eropa</a>.</p>\n<p style=\"margin: 0.5em 0px; line-height: 22.4px; color: #252525; font-family: sans-serif; font-size: 14px;\"><em>OSI Reference Model</em>&nbsp;pun akhirnya dilihat sebagai sebuah model ideal dari koneksi logis yang harus terjadi agar komunikasi data dalam jaringan dapat berlangsung. Beberapa protokol yang digunakan dalam dunia nyata, semacam&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"TCP/IP\" href=\"https://id.wikipedia.org/wiki/TCP/IP\">TCP/IP</a>,&nbsp;<a class=\"new\" style=\"text-decoration: none; color: #a55858; background: none;\" title=\"DECnet (halaman belum tersedia)\" href=\"https://id.wikipedia.org/w/index.php?title=DECnet&amp;action=edit&amp;redlink=1\">DECnet</a>&nbsp;dan&nbsp;<a style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"IBM\" href=\"https://id.wikipedia.org/wiki/IBM\">IBM</a>&nbsp;<a class=\"new\" style=\"text-decoration: none; color: #a55858; background: none;\" title=\"Systems Network Architecture (halaman belum tersedia)\" href=\"https://id.wikipedia.org/w/index.php?title=Systems_Network_Architecture&amp;action=edit&amp;redlink=1\">Systems Network Architecture</a>&nbsp;(SNA) memetakan tumpukan protokol (<em><a class=\"new\" style=\"text-decoration: none; color: #a55858; background: none;\" title=\"Protocol stack (halaman belum tersedia)\" href=\"https://id.wikipedia.org/w/index.php?title=Protocol_stack&amp;action=edit&amp;redlink=1\">protocol stack</a></em>) mereka ke<em>OSI Reference Model</em>.&nbsp;<em>OSI Reference Model</em>&nbsp;pun digunakan sebagai titik awal untuk mempelajari bagaimana beberapa protokol jaringan di dalam sebuah kumpulan&nbsp;<a class=\"mw-redirect\" style=\"text-decoration: none; color: #0b0080; background: none;\" title=\"Protokol jaringan\" href=\"https://id.wikipedia.org/wiki/Protokol_jaringan\">protokol</a>dapat berfungsi dan berinteraksi.</p>',1,'2016-03-25 06:35:38','2016-03-25 06:35:38');

/*Table structure for table `post_category` */

DROP TABLE IF EXISTS `post_category`;

CREATE TABLE `post_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_category_category_id_foreign` (`category_id`),
  KEY `post_category_post_id_foreign` (`post_id`),
  CONSTRAINT `post_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_category_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `post_category` */

insert  into `post_category`(`id`,`post_id`,`category_id`,`created_at`,`updated_at`) values (1,1,1,'2016-03-25 06:24:26','2016-03-25 06:24:26'),(2,1,2,'2016-03-25 06:24:26','2016-03-25 06:24:26'),(3,2,3,'2016-03-25 06:35:38','2016-03-25 06:35:38');

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_post_id_foreign` (`post_id`),
  CONSTRAINT `question_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `question` */

insert  into `question`(`id`,`content`,`post_id`,`created_at`,`updated_at`) values (1,'Apa kepanjangan HTML',1,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(2,'Versi HTML Terbaru',1,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(3,'Tag untuk gambar',1,'2016-03-25 06:34:16','2016-03-25 06:34:16'),(4,'Tag untuk memanggil CSS external',1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(5,'Fungsi tag tr ?',1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(6,'Kepanjangan tag ul',1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(7,'kepanjangan dari DOM',1,'2016-03-25 06:34:17','2016-03-25 06:34:17'),(8,'Kepanjangan OSI',2,'2016-03-25 06:39:02','2016-03-25 06:39:02'),(9,'Perangkat pada layer Network ?',2,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(10,'Kepanjangan TCP',2,'2016-03-25 06:39:03','2016-03-25 06:39:03'),(11,'Jumlah layer OSI',2,'2016-03-25 06:39:03','2016-03-25 06:39:03');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`photo`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'Diaz','Febrian','pictures/profile/user_1.jpg','testing@t.com','$2y$10$rYtlee7xav2Kc3ubhoQKguY1QtzTEN.1c0efjPCdMa9grFJjzO66G','ZK3IEmqrusVjlCmquENZfSrzbTgr3lbKn8umHH81VQqeNCejd3QdztYxFl2L','2016-03-25 06:21:13','2016-03-25 06:39:25'),(2,'Dummy','Pertama','pictures/profile/user_2.png','testing2@t.com','$2y$10$HVd/YfTuZL4rIKQS8wZ/Se56PAlwvb59OcOnloaY6sbWSdVSqUDky','XXIzIw1aoSbNVReH9xg1fZ86QXpVt0NGKGVSUV3YuZBFZEYQZvJcoHwutNjR','2016-03-25 06:40:06','2016-03-25 06:49:54'),(3,'Dummy','Kedua','pictures/profile/user_3.png','testing3@t.com','$2y$10$6kP/TghMjCEfknI2ZS81nuSLAshc14m.9V6c5.ssY.AxXz1TFUQu2','ind8Muzaoq7YZzYHlbWLS89TxHfeYfkr27VYVKRygcbWQ3Le03MiKfqSjZ5L','2016-03-25 06:41:02','2016-03-25 06:51:35'),(4,'Dummy','Ketiga','pictures/profile/user_4.png','testing4@t.com','$2y$10$d0EFD7Rpq8/Amli5jJemFO8LJmLexS2D5WVyVpeTnY3HN4RYkYvC2','5tjB3aX1IsVRTn9Snaxgdy8E5YIa390GKG2hEDjQ2zIUuXMMGs8Bbmzmc7i2','2016-03-25 06:49:36','2016-03-25 06:53:31');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
